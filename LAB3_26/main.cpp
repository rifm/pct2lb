#include <iostream>
#include <cmath>
#include <iomanip>
#include <string>

using namespace std;

class Point {
public:
    int x, y;

    Point(int _x = 0, int _y = 0) {
        x = _x;
        y = _y;
    }
};

class Triangle {
public:

    static int CountObj;

    Triangle() {
        CountObj++;
        return;
    };

    Triangle(double _a, double _b, double _c) {
        CountObj++;
        a = _a;
        b = _b;
        c = _c;
        return;
    };

    Triangle(double _a, double _b, double _c, double _p) {
        CountObj++;
        a = _a;
        b = _b;
        c = _c;
        if (CalculateHalfPerimeter() != _p) {
            cout << "Ошибка в параметре полупериметра: " << p << " != " << _p << endl;
        } else {
            p = _p;
        }
        return;
    };

    ~Triangle() {
        CountObj--;
        cout << "Объект успешно удален. Осталось объектов: " << CountObj << endl;
        return;
    };

    void Print();

    void Set(double _a, double _b, double _c) {
        a = _a;
        b = _b;
        c = _c;
    };

    double GetSquare() {
        return CalculateSquare();
    }

private:
    string description = "Класс 'Треугольник'. Номер " + to_string(CountObj);

protected:
    double a, b, c;
    // Высота
    double height_c;
    // Медиана
    double median_c;
    // Площадь
    double square;
    // Полупериметр
    double p;

    double CalculateHalfPerimeter() {
        p = ((a + b + c) / 2.0);
        return p;
    };

    double CalculateSquare() {
        CalculateHalfPerimeter();
        square = sqrt(p * (p - a) * (p - b) * (p - c));
        return square;
    };

    double CalculateHeight() {
        height_c = (2 * CalculateSquare()) / c;
        return height_c;
    };

    double CalculateMedian() {
        median_c = (1 / 2) * (sqrt(2 * (pow(a, 2) + pow(b, 2)) - pow(c, 2)));
        return median_c;
    };
};

void Triangle::Print() {
    cout << endl;
    cout << description << endl;
    cout << "Сторона a: " << a << endl;
    cout << "Сторона b: " << b << endl;
    cout << "Сторона c: " << c << endl;
    cout << "Полупериметр: " << CalculateHalfPerimeter() << endl;
    cout << "Площадь: " << CalculateSquare() << endl;
    cout << "Высота: " << CalculateHeight() << endl;
    cout << "Медиана: " << CalculateMedian() << endl;
    cout << endl;
}

class MagicTriangle : public Triangle {
public:

    MagicTriangle() {
        return;
    };

    MagicTriangle(double _a, double _b, double _c, Point _position, int _borderWidth) : Triangle(_a, _b, _c) {
        position = _position;
        borderWidth = _borderWidth;
        return;
    };

    ~MagicTriangle() {
        cout << "Магический треугольник удален" << endl;
        return;
    };

    void Print() {
        cout << endl;
        cout << description << endl;
        cout << "Сторона a: " << a << endl;
        cout << "Сторона b: " << b << endl;
        cout << "Сторона c: " << c << endl;
        cout << "Полупериметр: " << CalculateHalfPerimeter() << endl;
        cout << "Площадь: " << CalculateSquare() << endl;
        cout << "Высота: " << CalculateHeight() << endl;
        cout << "Медиана: " << CalculateMedian() << endl;
        cout << "Позиция x: " << position.x << endl;
        cout << "Позиция y: " << position.y << endl;
        cout << "Ширина обводки: " << borderWidth << endl;
        cout << endl;
    }

protected:
    int borderWidth;
    Point position;
private:

    string description = "Класс 'МагическийТреугольник'. Номер " + to_string(CountObj);
};

int Triangle::CountObj = 0;

ostream &operator<<(ostream &os, Point const &n) {
    os << "{x: " << n.x << ", y: " << n.y << "}";
    return os;
}

int main() {

    cout << "Статическая переменная Triangle::CountObj = " << Triangle::CountObj << endl;

    Triangle triangle1;
    Triangle triangle2(3, 4, 5);

    /*
    //error: 'description' is a private member of 'Triangle'
    //note: declared private here
    cout << triangle2.description << endl;
    */

    Triangle triangle3(3, 4, 5, 6);

    triangle2.Print();
    triangle3.Print();

    triangle1.Set(3, 4, 5);
    triangle1.Print();

    Triangle *pointerToTriangle1 = &triangle1;
    cout << "Площадь треугольника номер 1: " << pointerToTriangle1->GetSquare() << endl;

    Triangle *dynamicTriangle = new Triangle(3, 4, 5);
    dynamicTriangle->Print();
    delete dynamicTriangle;

    Triangle triangles[30];
    triangles[6] = Triangle(6, 7, 8);
    triangles[6].Print();

    cout << "Статическая переменная Triangle::CountObj = " << Triangle::CountObj << endl;

    Point magicTrianglePosition = Point(6, 7);

    MagicTriangle magicTriangle1(3, 4, 5, magicTrianglePosition, 8);
    magicTriangle1.Print();
    magicTriangle1.Set(8, 15, 17);
    cout << "Площадь объекта наследника: " << magicTriangle1.GetSquare();

    MagicTriangle magicTriangles[30];
    magicTriangles[6] = magicTriangle1;
    magicTriangles[6].Print();

    /*
    //error: 'a' is a protected member of 'Triangle'
    //note: declared protected here
    cout << magicTriangle1.a << endl;
     */

    cout << "Позиция магического треугольника: " << endl;
    cout << magicTrianglePosition << endl;

    return 0;
}
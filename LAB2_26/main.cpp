#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include "second.cpp"

using namespace std;

void Vector_Input();
void Basic_Istream();

extern int iSecond;

int main() {

    int iVal;
    double dVal;
    char sArr[100];

    //Vector_Input();

    //Basic_Istream();

    /*
    Rect rect1(10, 5);
    rect1.Print();
    rect1.SetWH();
    rect1.Print();*/

    //cout << Spravka() << endl;
    //cout << TodayIs() << endl;

    cout << "iSecond = " << iSecond << endl;

    int num = 5;
    int *pointerToNum = &num;

    cout << "num = " << num << endl;
    cout << "Указатель на num = " << *pointerToNum << endl;

    cout << "Введите iVal: " << endl;
    cin >> iVal;
    cout << "Введено iVal = " << iVal << endl;
    cout << "hex iVal = " << hex << iVal << endl;
    cout << "dec iVal = " << dec << iVal << endl;
    cout << "oct iVal = " << oct << iVal << endl;
    cout.setf(ios_base::hex);
    cout << "Повторно после вызова cout.setf(ios_base::hex) ival =  " << iVal << endl;
    cout.unsetf(ios_base::hex);
    cout << "Повторно после вызова cout.unsetf(ios_base::hex) ival =  " << iVal << endl;
    cout << "setbase(16) для " << iVal << " = " << setbase(16) << iVal << endl;
    cout << "setbase(10) для " << iVal << " = " << setbase(10) << iVal << endl;
    cout << "setbase(8) для " << iVal << " = " << setbase(8) << iVal << endl;
    cout << "showbase для " << iVal << " hex - " << showbase << hex << iVal << endl;
    cout << "showbase для " << iVal << " oct - " << showbase << oct << iVal << endl;
    cout << "showbase для " << iVal << " dec - " << showbase << dec << iVal << endl;

    cout << "Введите dVal: " << endl;
    cin >> dVal;
    cout << "Введено dVal = " << dVal << " = " << scientific << dVal << endl;
    cout << "Пример после использования precision(20): " << setprecision(2) << dVal << endl;
    cout << "До setprecision(8): " << dVal << endl;
    cout << "После setprecision(8): " << setprecision(8) << dVal << endl;

    cout << "Введите sArr: " << endl;
    cin >> sArr;
    cout << "Введено sArr = " << sArr << endl;
    cout << "Пример sArr после width(20): " << setw(20) << sArr << endl;
    cout << "Пример sArr после width(20): " << setfill('*') << setw(20) << sArr << endl;
    cout << "Пример sArr после width(20): " << setfill('!') << setw(30) << sArr << endl;
    cout << left << setw(30) << setfill('!') << sArr << " для sMas после left в потоке" << endl;
    cout << right << setw(30) << setfill('!') << sArr << " для sMas после right в потоке" << endl;

    return 0;
}

void Vector_Input(){
    vector<string> Strings;
    int vectorSize;

    cout << "Введите размер вектора" << endl;
    cin >> vectorSize;
    for(int i = 0; i<=vectorSize-1; i++){
        string String;
        cin >> String;
        Strings.push_back(String);
    }

    for(int i = 0; i<=vectorSize-1; i++){
        cout << "Элемент вектора " << i << ": " << Strings[i] << endl;
    }
    return;
}

void Basic_Istream(){

    // getline
    cout << "getline" << endl;
    char line[256];
    cin.getline(line, 256);
    cout << "Вы ввели: " << line << endl;

    // get
    cout << "get" << endl;
    char c;
    cin.get(c);
    cout << "Вы ввели: " << c << endl;

    // putchar
    char schar = 'h';
    putchar(schar);
    cout << endl;

    // puts
    char string [] = "Hello world!";
    puts (string);
    cout << endl;

    return;
};
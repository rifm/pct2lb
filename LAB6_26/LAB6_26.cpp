/*
* float - ��� ����������
* 2 - ����� ���������� �������
* ����� - ���������
* I, J, K, L - ����������� ������ (�����)
*/

#include <iostream>
#include "stdafx.h"
#include "LAB6_26.h"

using namespace std;

int main() {

	Base *baseObject;

	Type1 T1;
	Type2 T2;

	baseObject = &T1;

	switch (baseObject->type) {
	case 1:
		((Type1 *)baseObject)->move(5);
		break;
	case 2:
		((Type2 *)baseObject)->move(5);
		break;
	}

	Abstract *pAbs;
	Deriv1 d1(33);
	Deriv2 d2(44);
	Deriv3 d3(66);

	d1.print();
	d2.print();
	d3.print();

	Deriv1 *pD1 = &d1;
	Deriv2 *pD2 = &d2;
	Deriv3 *pD3 = &d3;

	pD1->print();
	pD2->print();
	pD3->print();

	pAbs = &d1;
	pAbs->print();
	pAbs = &d2;
	pAbs->print();
	pAbs = &d3;
	pAbs->print();

	CObList ListDer;

	cout << "CObList" << endl;

	ListDer.AddTail((CObject *)&d1);
	ListDer.AddTail((CObject *)&d2);
	ListDer.AddTail((CObject *)&d2);
	ListDer.AddTail((CObject *)&d1);
	ListDer.AddTail((CObject *)&d2);
	ListDer.AddTail((CObject *)&d3);

	POSITION pos;

	for (pos = ListDer.GetHeadPosition(); pos != NULL;){
		((Deriv1 *)(ListDer.GetNext(pos)))->print();
	}

	ListDer.RemoveAt(ListDer.GetHeadPosition());

	cout << "CObList after one element deletion" << endl;

	for (pos = ListDer.GetHeadPosition(); pos != NULL;){
		((Deriv1 *)(ListDer.GetNext(pos)))->print();
	}

	ListDer.RemoveAll();

	cout << "CObList after remove all" << endl;

	for (pos = ListDer.GetHeadPosition(); pos != NULL;){
		((Deriv1 *)(ListDer.GetNext(pos)))->print();
	}

	I *iAbs;
	J j(1);

	j.print();

	system("PAUSE");

	return 0;
}
#pragma once

#include <iostream>
#include "resource.h"

using namespace std;

class I : CObject{
public:
	float v;
	I(){};
	I(float _v){ v = _v; };
	virtual void print() = 0;
};

class J : public I{
public:
	float vJ;
	J(){};
	J(float _v){ vJ = _v; };
	virtual void print(){
		cout << "J = " << vJ << "; I = " << v << endl;
	};
};

class K : public J{
public:
	float vK;
	K(){};
	K(float _v){ vK = _v; };
	virtual void print(){
		cout << "K = " << vJ << "; I = " << v << endl;
	};
};

class L : public J{
public:
	float vL;
	L(){};
	L(float _v){ vL = _v; };
	virtual void print(){
		cout << "L = " << vJ << "; I = " << v << endl;
	};
};

class Abstract : CObject{
public:
	float fPar;
	Abstract(){};
	Abstract(float _fPar){
		fPar = _fPar;
	};
	virtual void print() = 0;
private:
protected:
};

class Deriv1 : public Abstract{
public:
	float fDPar;
	Deriv1(){};
	Deriv1(float _fDPar){
		fDPar = _fDPar;
	};
	virtual void print(){
		cout << "Deriv1 = " << fDPar << "; Abstract = " << fPar << endl;
	};
private:
protected:
};

class Deriv2 : public Deriv1{
public:
	float fDPar;
	Deriv2(){};
	Deriv2(float _fDPar){
		fDPar = _fDPar;
	};
	virtual void print(){
		cout << "Deriv2 = " << fDPar << "; Abstract = " << fPar << endl;
	};
private:
protected:
};

class Deriv3 : public Deriv1{
public:
	float fDPar;
	Deriv3(){};
	Deriv3(float _fDPar){
		fDPar = _fDPar;
	};
	virtual void print(){
		cout << "Deriv3 = " << fDPar << "; Abstract = " << fPar << endl;
	};
private:
protected:
};

class Base {
public:
	Base() {
		a = 0;
		type = 0;
	};

	Base(int x) {
		a = x;
		type = 0;
	};
	int a, type;
private:
protected:
};

class Type1 : public Base {
public:
	Type1() {
		coordinate = 0;
		type = 1;
	}

	Type1(int x) {
		coordinate = x;
		type = 1;
	}

	void move(int delta) {
		coordinate = coordinate + delta;
	}

	int coordinate;
private:
protected:
};

class Type2 : public Base {
public:
	Type2() {
		coordinate = 0;
		type = 2;
	}

	Type2(int x) {
		coordinate = x;
		type = 2;
	}

	void move(int delta) {
		coordinate = coordinate - delta;
	}

	int coordinate;
private:
protected:
};
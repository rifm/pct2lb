// LAB5_26.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "LAB5_26.h"

/*
* ������� - ��� ������� ��� ���������� ���������
* ������� - ��� ������� ��� ���������� �������
* Complex - ��� ������ ��� ���������� ��������
* ���� >> - ��� �������� ��� ���������� ����� � ������
* */


/*
* ������� ���������� ������� ��-��������� � ���������
* */

#include <iostream>

using namespace std;

class Cmplx {
public:

	Cmplx() { }

	/*
	* ��� ������������� �������
	* */


	friend const Cmplx operator+(Cmplx &c, int i) {
		c.x = i + abs(c.x);
		c.y = i + abs(c.y);
		return c;
	};

	friend const Cmplx operator-(int i, Cmplx &c) {
		c.x = i - c.x;
		c.y = i - c.y;
		return c;
	};

	friend const Cmplx operator++(Cmplx &c, int i) {
		c.operators++;
		return c;
	};

	friend const Cmplx operator--(Cmplx &c, int);

	bool Cmplx::operator < (const Cmplx &c){
		return (c.x > x) && (c.y > y);
	}

	friend Cmplx& operator+=(Cmplx &c, int num){
		c.x += num;
		c.y += num;
	}

	Cmplx(int _x, int _y) : operators(_x) {
		x = _x;
		y = _y;
	}

	void Print(){
		cout << "X: " << x << endl;
		cout << "Y: " << y << endl;
	}
	
private:
	int x, y, operators = 0;
protected:
};

const Cmplx operator--(Cmplx &�, int i) {
	�.operators--;
	return �;
}

class CmplxCpy {
public:

	CmplxCpy(){

	}

	CmplxCpy(int _x, int _y) {
		x = _x;
		y = _y;
	}

	friend const CmplxCpy operator*(CmplxCpy &c, int i) {
		c.x = i * -abs(c.x);
		c.y = i * -abs(c.y);
		return c;
	};

	friend const CmplxCpy operator-(CmplxCpy &c, int i) {
		c.x = i + -abs(c.x);
		c.y = i + -abs(c.y);
		return c;
	};

	friend istream &operator>>(istream &stream, CmplxCpy &obj) {
		stream >> obj.x;
		stream >> obj.y;
		return stream;
	}

	int GetX(){
		return x;
	}

	int GetY(){
		return y;
	}

	void SetY(int _y){
		y = _y;
	}

	void SetX(int _x){
		x = _x;
	}

private:
	int x, y;
};

class CmplxCpyHeir : public CmplxCpy{
public:

	CmplxCpyHeir(int _a, int _b){
		a = _a;
		b = _b;
		SetX(0);
		SetY(0);
	}

	CmplxCpyHeir(int _a, int _b, int _x, int _y) : CmplxCpy(_x, _y) {
		a = _a;
		b = _b;
	}

	/*
	friend istream &operator>>(istream &stream, CmplxCpyHeir &obj) {
		stream >> obj.x;
		stream >> obj.y;
		return stream;
	}

	friend const CmplxCpyHeir operator*(CmplxCpyHeir &c, int i) {
		c.x = i * abs(c.x);
		c.y = i * abs(c.y);
		return c;
	};*/

	void Print(){
		cout << "A: " << a << endl;
		cout << "B: " << b << endl;
		cout << "X: " << GetX() << endl;
		cout << "Y: " << GetY() << endl;
	}

private:
	int a, b;
protected:
};

float _minimumGlobal = 0;

float minimum(float a = 0, float b = 0, float c = 0);

/*
* error: redefinition of default argument
* float minimum1(float a = 0, float b, float c);
*/

float minimum1(float a, float b, float c);

float minimumGlobal(float a, float b, float c = _minimumGlobal);

float average(float, float);

float average(float, float, float);

float average(float, float, float, float);

float averageType(int, int);

float averageType(int, float);

float averageType(float, float);

int main() {

	setlocale(LC_ALL, "Russian");

	Cmplx cmplx1(1, 2);
	Cmplx cmplx2(1, 2);
	cout << "��: " << endl;
	cmplx1.Print();
	cmplx1 = 5 - cmplx2;
	cout << "�����: " << endl;
	cmplx1.Print();

	cout << "���������� " << endl;
	bool compare = Cmplx(0, 0) < Cmplx(0,1);
	cout << "Compared: " << compare << endl;

	CmplxCpyHeir c1(0, 1);
	cout << c1.GetX() << " " << c1.GetY();
	c1 * 2;
	cout << endl;
	cout << c1.GetX() << " " << c1.GetY();

	CmplxCpyHeir cmplxheir1(1, 2, 3, 4);
	cmplxheir1.Print();

	system("PAUSE");

	return 0;
}

float minimum(float a, float b, float c) {
	float min = c;
	if (min > a) { min = a; }
	if (min > b) { min = b; }
	return min;
}

float minimum1(float a = 0, float b = 0, float c = 0) {
	return minimum(a, b, c);
}

float minimumGlobal(float a = 0, float b = 0, float c = 0) {
	return minimum(a, b, c);
}

float average(float a, float b) {
	return (a + b) / 2;
}

float average(float a, float b, float c) {
	return (a + b + c) / 3;
}

float average(float a, float b, float c, float d) {
	return (a + b + c + d) / 4;
}

float averageType(int a, int b) {
	return (a + b) / 2;
}

float averageType(int a, float b) {
	return (a + b) / 2;
}

float averageType(float a, float b) {
	return (a + b) / 2;
}
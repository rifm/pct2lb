#include "stdafx.h"
#include "LAB4_26.h"

#include <iostream>

using namespace std;

#define if ����

#define Swap2(_a, _b) \
    a = _a, b = _b, tmp; \
    tmp = a; \
    a = b; \
    b = tmp; \


template<typename Type>
void Swap(Type &a, Type &b) {
	Type c(a);
	a = b;
	b = c;
}

template<typename Type>
Type Sum(int size, Type *arr) {
	Type result = 0;
	for (int i = 0; i <= size - 1; i++) {
		result += arr[i];
	}
	return result;
}

template<typename T>
class Rectangl {
public:
	Rectangl() {
		return;
	}

	Rectangl(T _x1, T _x2, T _y1, T _y2, int _color, int _border) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
		color = _color;
		border = _border;
		return;
	}

	void Print() {
		cout << "�������������" << endl;
		cout << "����������: " << color << endl;
		cout << "������� �����: " << border << endl;
		cout << "x1: " << x1 << endl;
		cout << "x2: " << x2 << endl;
		cout << "y1: " << y1 << endl;
		cout << "y2: " << y2 << endl;
		return;
	}

	void Move(T _x1, T _x2, T _y1, T _y2) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
	}

	void SetFill(int _color) {
		color = _color;
	}

protected:
	int color, border;
	T x1, x2, y1, y2;
};

template<typename T, int _border>
class SecondRectangl {
public:
	SecondRectangl() {
		return;
	}

	SecondRectangl(T _x1, T _x2, T _y1, T _y2, int _color) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
		color = _color;
		border = _border;
		return;
	}

	void Move(T, T, T, T);

	void Print() {
		cout << "������ �������������" << endl;
		cout << "����������: " << color << endl;
		cout << "������� �����: " << border << endl;
		cout << "x1: " << x1 << endl;
		cout << "x2: " << x2 << endl;
		cout << "y1: " << y1 << endl;
		cout << "y2: " << y2 << endl;
		return;
	}

protected:
	int color, border;
	T x1, x2, y1, y2;
};

template<>
class SecondRectangl<int, 5> {
public:
	SecondRectangl() {
		return;
	}

	SecondRectangl(int _x1, int _x2, int _y1, int _y2, int _color) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
		color = _color;
		border = 5;
		return;
	}

	void Print() {
		cout << "������ ������������� � ���������������� ����� int" << endl;
		cout << "����������: " << color << endl;
		cout << "������� �����: " << border << endl;
		cout << "x1: " << x1 << endl;
		cout << "x2: " << x2 << endl;
		cout << "y1: " << y1 << endl;
		cout << "y2: " << y2 << endl;
		return;
	}

protected:
	int color, border;
	int x1, x2, y1, y2;
};

template<typename T, int _border>
void SecondRectangl<T, _border>::Move(T _x1, T _x2, T _y1, T _y2) {
	x1 = _x1;
	x2 = _x2;
	y1 = _y1;
	y2 = _y2;
}

class ThirdRectangl : public SecondRectangl<long, 2> {
public:
	ThirdRectangl() {
		return;
	}

	ThirdRectangl(long _x1, long _x2, long _y1, long _y2, int _color) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
		color = _color;
		return;
	}

	void Print() {
		cout << "������ �������������" << endl;
		cout << "����������: " << color << endl;
		cout << "x1: " << x1 << endl;
		cout << "x2: " << x2 << endl;
		cout << "y1: " << y1 << endl;
		cout << "y2: " << y2 << endl;
		return;
	}

private:
protected:
};

template<class T>
class ForthRectangle : public SecondRectangl < T, 2 > {
public:
	ForthRectangle() :SecondRectangl(){};

	ForthRectangle(long _x1, long _x2, long _y1, long _y2, int _color) {
		x1 = _x1;
		x2 = _x2;
		y1 = _y1;
		y2 = _y2;
		color = _color;
		return;
	}

};


typedef SecondRectangl<int, 2> typedefRectangle;

int main() {

	setlocale(LC_ALL, "Russian");

	int x = 5, y = 10;
	float d1 = 5.2, d2 = 10.7;

	cout << "����� ���:" << endl;
	cout << "�� Swap: " << x << " " << y << endl;
	Swap(x, y);
	cout << "����� Swap: " << x << " " << y << endl;

	cout << "������������ ���:" << endl;
	cout << "�� Swap: " << d1 << " " << d2 << endl;
	Swap(d1, d2);
	cout << "����� Swap: " << d1 << " " << d2 << endl;

	int iArr[5] = { 3, 4, 5, 6, 7 };
	float fArr[5] = { 3.1, 4.1, 5.1, 6.1, 7.1 };
	double dArr[5] = { 3.3, 4.3, 5.3, 6.3, 7.3 };

	cout << "����� int = " << Sum(5, iArr) << endl;
	cout << "����� float = " << Sum(5, fArr) << endl;
	cout << "����� double = " << Sum(5, dArr) << endl;

	Rectangl<int> intRectangl(1, 2, 3, 4, 5, 6);
	Rectangl<float> floatRectangl(1.1, 2.2, 3.3, 4.4, 5, 6);
	Rectangl<double> doubleRectangl(1.2, 2.3, 3.4, 4.5, 5, 6);

	cout << endl;
	intRectangl.Print();
	cout << endl;
	floatRectangl.Print();
	cout << endl;
	doubleRectangl.Print();
	cout << endl;

	cout << "!������ ������� ����������������!" << endl;
	SecondRectangl<int, 5> intSecondRectangle(1,2,3,4,5);
	intSecondRectangle.Print();

	SecondRectangl<double, 5> doubleSecondRectangl(1.2, 2.3, 3.4, 4.5, 5);
	doubleSecondRectangl.Print();
	doubleSecondRectangl.Move(5.2, 6.3, 7.4, 8.5);
	cout << "����� �����������" << endl;
	doubleSecondRectangl.Print();

	ThirdRectangl thirdRectangl(1, 2, 3, 4, 5);
	thirdRectangl.Print();

	ForthRectangle<int> intForthRectangle(1,2,3,4,5);

	typedefRectangle typeDEFRect(1,2,3,4,5);

	system("PAUSE");
	return 0;
}
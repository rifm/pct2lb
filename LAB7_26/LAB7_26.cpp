/*
* Triangle - ������������ ������
* CArray - ��� ������������� ������
* ������/��������� - �������� � ���������� � ������
* int - ��� �������
*
* �� �������:
* ��������� �������� ������ MFC ��� ATL �� �������� ��� ������ ������
*
* ����� VS
*
* */

#include "stdafx.h"
#include "LAB7_26.h"
#include <iostream>
#include <vector>

using namespace std;

class Triangle {
public:
	Triangle() {

	}

	Triangle(int _a, int _b, int _c) {
		a = _a;
		b = _b;
		c = _c;
	}

	friend ostream& operator<<(ostream& os, const Triangle& obj);

	void printTriangle();

private:
protected:
	int a, b, c;
};

ostream& operator<<(ostream& os, const Triangle& obj){
	os << "Triangle from CArray: " << endl;
	os << "A: " << obj.a << endl;
	os << "B: " << obj.b << endl;
	os << "C: " << obj.c << endl;
	return os;
}

void printVector(vector<int> &v);

void printVectorWithIterator(vector<int> &v);

void printVectorTriangleWithIterator(vector<Triangle> &v);

void addToHTVector(vector<int> &v, int num, bool head = true);

void addToHTTriangleVector(vector<Triangle> &v, Triangle triangle, bool head = true);

void removeFromHTVector(vector<int> &v, bool head = true);

void removeFromHTTriangleVector(vector<Triangle> &v, bool head = true);

void isVectorEmpy(vector<int> &v);

void isTriangleVectorEmpy(vector<Triangle> &v);

int main() {

	vector<int> integerVector;

	// 1 2 3 4 5
	integerVector.push_back(1);
	integerVector.push_back(2);
	integerVector.push_back(3);
	integerVector.push_back(4);
	integerVector.push_back(5);

	printVector(integerVector);

	// 6 1 2 3 4 5
	addToHTVector(integerVector, 6, true);

	printVector(integerVector);

	// 6 1 2 3 4 5 7
	addToHTVector(integerVector, 7, false);

	printVector(integerVector);

	// 6 1 2 3 4 5
	removeFromHTVector(integerVector, false);

	printVector(integerVector);

	isVectorEmpy(integerVector);

	vector<Triangle> triangles;
	triangles.push_back(Triangle(1, 2, 3));
	triangles.push_back(Triangle(3, 4, 5));
	triangles.push_back(Triangle(5, 6, 7));
	triangles.push_back(Triangle(7, 8, 9));
	triangles.push_back(Triangle(9, 10, 11));

	addToHTTriangleVector(triangles, Triangle(11, 12, 13));

	printVectorTriangleWithIterator(triangles);

	CArray<Triangle, Triangle> trianglesCArray;

	trianglesCArray.Add(Triangle(1, 2, 3));
	trianglesCArray.Add(Triangle(2, 3, 4));
	trianglesCArray.Add(Triangle(3, 4, 5));

	cout << trianglesCArray.GetAt(2);

	trianglesCArray.SetAt(2, Triangle(4, 5, 6));

	cout << trianglesCArray.GetAt(2);

	trianglesCArray.RemoveAt(trianglesCArray.GetSize() - 1);

	printVectorTriangleWithIterator(triangles);

	CArray<Triangle, Triangle> newTrianglesCArray;
	newTrianglesCArray.Add(Triangle(5, 6, 7));

	cout << "Size before: " << trianglesCArray.GetSize() << endl;

	trianglesCArray.Copy(newTrianglesCArray);

	cout << "Size after: " << trianglesCArray.GetSize() << endl;

	cout << "Final array:" << endl ;
	for (int i = 0; i <= trianglesCArray.GetSize() - 1; i++){
		cout << trianglesCArray.GetAt(i);
	};

	system("PAUSE");

	return 0;
}

void Triangle::printTriangle(){
	cout << "Triangle: " << endl;
	cout << "A: " << a << endl;
	cout << "B: " << b << endl;
	cout << "C: " << c << endl;
}

void printVector(vector<int> &v) {
	cout << "Vector elements:" << endl;
	for (int i = 0; i <= v.size() - 1; i++) {
		cout << v[i] << endl;
	}
}

void printVectorWithIterator(vector<int> &v) {
	vector<int>::iterator it;
	cout << "Vector elements with iterator:" << endl;
	for (it = v.begin(); it != v.end(); it++) {
		cout << *it << endl;
	}
}

void printVectorTriangleWithIterator(vector<Triangle> &v) {
	vector<Triangle>::iterator it;
	cout << "Vector elements with iterator:" << endl;
	for (it = v.begin(); it != v.end(); it++) {
		it->printTriangle();
	}
}

/*
* HT for Head and Tail
* */
void addToHTVector(vector<int> &v, int num, bool head) {
	if (head) {
		v.insert(v.begin(), num);
	}
	else {
		v.insert(v.end(), num);
	}
}

/*
* HT for Head and Tail
* */
void addToHTTriangleVector(vector<Triangle> &v, Triangle triangle, bool head) {
	if (head) {
		v.insert(v.begin(), triangle);
	}
	else {
		v.insert(v.end(), triangle);
	}
}

/*
* HT for Head and Tail
* */
void removeFromHTVector(vector<int> &v, bool head) {
	if (head) {
		v.erase(v.begin());
	}
	else {
		v.erase(v.end() - 1);
	}
}

/*
* HT for Head and Tail
* */
void removeFromHTTriangleVector(vector<Triangle> &v, bool head) {
	if (head) {
		v.erase(v.begin());
	}
	else {
		v.erase(v.end() - 1);
	}
}

void isVectorEmpy(vector<int> &v) {
	cout << "Is vector empty? " << v.empty() << endl;
	v.clear();
	cout << "Is vector empty? " << v.empty() << endl;
}

void isTriangleVectorEmpy(vector<Triangle> &v) {
	cout << "Is triangle vector empty? " << v.empty() << endl;
	v.clear();
	cout << "Is triangle vector empty? " << v.empty() << endl;
}